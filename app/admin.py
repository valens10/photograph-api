from django.contrib import admin
from .models import *

# Register your models here.

admin.site.register(VideoAlbum)
admin.site.register(Video)

admin.site.register(AlbumPhoto)
admin.site.register(Photo)

admin.site.register(Blog)
admin.site.register(Message)
from django.db import models
from django.contrib.auth.models import User
import datetime

# Create your models here.

class AlbumPhoto(models.Model):
    name = models.CharField(max_length=200, blank=False, null=False)
    thundernail = models.ImageField(upload_to='images/')
    description = models.TextField(blank=True, null=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return str(self.name)
    
class Photo(models.Model):
    album = models.ForeignKey(AlbumPhoto, on_delete=models.CASCADE, blank=False, null=False)
    image = models.ImageField(upload_to='images/')
    created_at = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return str(self.album.name)
    
    
class Blog(models.Model):
    title = models.CharField(max_length=250)
    image = models.ImageField(upload_to='images/')
    category = models.CharField(max_length=200, default='NEWS PORTRAITS')
    body = models.TextField(blank=False, null=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.title
    
class Message(models.Model):
    name = models.CharField(max_length=300, blank=False, null=False)
    email = models.CharField(max_length=250, null=False, blank=False)
    message = models.TextField(blank=False,null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.name
    
class VideoAlbum(models.Model):
    name = models.CharField(max_length=300, blank=False, null=False)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.name
    
class Video(models.Model):
    videoAlbum = models.ForeignKey(VideoAlbum, on_delete=models.CASCADE, blank=False, null=False)
    file = models.FileField(upload_to='images/videos/', blank=False, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return str(self.created_at)
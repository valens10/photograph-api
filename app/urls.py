from django.urls import path
from .views import *

urlpatterns = [
    path('album-photo/', AlbumPhotoList.as_view(), name='album_photo_list'),
    path('album-photo/<slug:pk>', AlbumPhotoDetail.as_view(), name='album_photo_detail'),
    
    path('photos/', PhotoList.as_view(), name='photo_list'),
    path('photos/<slug:pk>', PhotoDetail.as_view(),name='photo_detail'),
    
    path('album-video/', VideoAlbumList.as_view(), name='video_list'),
    path('album-video/<slug:pk>', VideoAlbumDetail.as_view(),name='video_album_detail'),

    path('videos/', VideoList.as_view(), name='video_list'),
    path('videos/<slug:pk>', VideoDetail.as_view(),name='video_detail'),
    
    path('blog/', BlogList.as_view(), name='blog_list'),
    path('blog/<slug:pk>', BlogDetail.as_view(),name='blog_detail'),
    
    path('message/', MessageList.as_view(), name='message_list'),
    path('message/<slug:pk>', MessageDetail.as_view(),name='message_detail'),
]
